var expect = require('chai').expect;
var handler = require('../index').handler;
var _ = require('../utils');

describe('register', function() {
  this.timeout(20000);
  var params = {
    Password: _.token(),
    Email: '',
    AppID: '',
  };
  var register = function(succeed, fail) {
    handler({
      method: 'User.Register',
      params: [params],
      id: _.token(),
    }, {
      succeed: succeed,
      fail: fail,
    });
  };

  describe('valid appID', function() {
    before(function() {
      params.AppID = 'test';
    });

    describe('pristine user', function() {
      before(function() {
        params.Email = _.token() + '@mailinator.com';
      });

      it('should succeed with an empty response.', function(done) {
        register(function(res) {
          expect(res).to.deep.equal({});
          done();
        }, done);
      });

      it('should fail with ErrRegistrationPending on successive call.', function(done) {
        register(noSucceed(done), function(err) {
          expect(err).to.equal(_.errors.RegistrationPending);
          done();
        });
      });
    });
  });

  describe('invalid appID', function() {
    before(function() {
      params.AppID = _.token();
    });

    it('should fail.', function(done) {
      register(noSucceed(done), function(err) {
        expect(err).to.equal(_.errors.NoApp);
        done();
      });
    });
  });
});

function noSucceed(done) {
  return function() {
    console.log(arguments);
    done(new Error('success where an error was expected'));
  };
}
