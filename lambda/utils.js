require('es6-promise').polyfill();
var randomstring = require('randomstring');
var _ = require('lodash');

exports.token = function(length) {
  return randomstring.generate(length || 12);
};

exports.errors = {
  WrongPassword: 'ErrWrongPassword',
  RegistrationPending: 'ErrRegistrationPending',
  RegistrationNotPending: 'ErrRegistrationNotPending',
  istered: 'ErrRegistered',
  NotRegistered: 'ErrNotRegistered',
  NoApp: 'ErrNoApp',
};

module.exports = _.assign(_, exports);
