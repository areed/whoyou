var net = require('net');
var spawn = require('child_process').spawn;
var _ = require('./utils');

var table = {};

//https://aws.amazon.com/blogs/compute/running-executables-in-aws-lambda/
process.env['PATH'] = process.env['PATH'] + ':' + process.env['LAMBDA_TASK_ROOT'];

var client = new Promise(function(resolve, reject) {
    var whoyou = spawn('./whoyou');

    whoyou.stdout.on('data', function(buf) {
      resolve(buf.toString('utf8'));
    });

    whoyou.stderr.on('data', function(buf) {
      console.log(buf.toString('utf8'));
    });

    whoyou.on('close', function(code, signal) {
      console.log('whoyou binary exited', code, signal);
      reject();
    });

    whoyou.on('error', function(err) {
      console.log('whoyou binary error');
      console.log(err);
      reject();
    });
  })
  .then(function(tmpfile) {
    return new Promise(function(resolve, reject) {
      var socket = net.connect(tmpfile, function() {
        resolve(socket);
      });

      socket.on('error', function(err) {
        console.log('socket error', err);
      });

      socket.on('data', function(buf) {
        var response = JSON.parse(buf.toString('utf8'));
        var callback = table[response.id];

        if (callback) {
          delete table[response.id];
          callback(response);
        }
      });
    });
  });

exports.handler = function(request, reply) {
  request.jsonrpc = '2.0';

  table[request.id] = function(response) {
    if (response.error) {
      reply.fail(response.error);
      return;
    }

    reply.succeed(response.result);
  };

  client.then(function(client) {
    client.write(JSON.stringify(request));
  });
};
