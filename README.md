# whoyou.io
This program is an authentication service with JWTs.

## Continuous Integration and Deployment
Use (GitLab Flow)[http://doc.gitlab.com/ee/workflow/gitlab_flow.html#issues-with-gitlab-flow] with GitLab CI.
1. Run go tests and mocha tests in separate jobs on every commit to every
   branch.
2. To deploy, merge into production and tag.
	- Merge into production.
	- Tag the merge commit in production.
	- Go install
	- mv ~/bin/linux_amd64/whoyou lambda/
	- zip -r lambda-{tag}.zip node_modules index.js utils.js package.json
	whoyou conf.json
	- create lambda function in console with code
	- create new aws api gateway using new lambda function
	- run integration tests against new api
	- point domain name at new api
	- run integration tests against domain name
