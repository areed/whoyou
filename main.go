package main

import (
	"net"
	"net/rpc"
	"net/rpc/jsonrpc"
	"os"

	"github.com/areed/token"

	whorpc "gitlab.com/areed/whoyou/apis/rpc"
	"gitlab.com/areed/whoyou/lib"
	"gitlab.com/areed/whoyou/log/dynamo"
)

func main() {
	lib.Log = dynamo.New()
	server := rpc.NewServer()
	server.Register(new(whorpc.User))

	t, err := token.New(12)
	if err != nil {
		panic(err)
	}
	f := "/tmp/" + t
	addr, err := net.ResolveUnixAddr("unix", f)
	if err != nil {
		panic(err)
	}
	listener, err := net.ListenUnix("unix", addr)
	if err != nil {
		panic(err)
	}
	_, err = os.Stdout.Write([]byte(f))
	if err != nil {
		panic(err)
	}
	for {
		conn, err := listener.Accept()
		if err != nil {
			panic(err)
		}
		go server.ServeCodec(jsonrpc.NewServerCodec(conn))
	}
}
