package dynamo

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"

	util "github.com/areed/dynamoutils"

	"gitlab.com/areed/whoyou/domain"
	"gitlab.com/areed/whoyou/log"
)

var usersTable = aws.String("whoyou-users")
var appsTable = aws.String("whoyou-apps")

type Dynamo struct {
	client *dynamodb.DynamoDB
}

func New() *Dynamo {
	return &Dynamo{
		client: dynamodb.New(session.New(), &aws.Config{Region: aws.String("us-west-2")}),
	}
}

func (d *Dynamo) SaveApp(app *domain.App) error {
	_, err := d.client.PutItem(&dynamodb.PutItemInput{
		TableName: appsTable,
		Item: map[string]*dynamodb.AttributeValue{
			"app_id":                     util.FormatString(app.ID),
			"origin":                     util.FormatString(app.Origin),
			"key":                        util.FormatString(app.Key),
			"signing_secret":             {B: app.SigningSecret},
			"bcrypt_rounds":              util.FormatInt(app.BcryptRounds),
			"jwt_term":                   util.FormatDuration(app.JWTTerm),
			"registration_term":          util.FormatDuration(app.RegistrationTerm),
			"recovery_term":              util.FormatDuration(app.RecoveryTerm),
			"verification_email_html":    util.FormatString(app.VerificationEmail.HTML),
			"verification_email_text":    util.FormatString(app.VerificationEmail.Text),
			"verification_email_sender":  util.FormatString(app.VerificationEmail.SenderEmail),
			"verification_email_subject": util.FormatString(app.VerificationEmail.Subject),
			"recovery_email_html":        util.FormatString(app.RecoveryEmail.HTML),
			"recovery_email_text":        util.FormatString(app.RecoveryEmail.Text),
			"recovery_email_subject":     util.FormatString(app.RecoveryEmail.Subject),
			"recovery_email_sender":      util.FormatString(app.RecoveryEmail.SenderEmail),
		},
	})
	return err
}

func (d *Dynamo) GetApp(id string) (*domain.App, error) {
	resp, err := d.client.GetItem(&dynamodb.GetItemInput{
		TableName:      appsTable,
		ConsistentRead: aws.Bool(true),
		Key: map[string]*dynamodb.AttributeValue{
			"app_id": util.FormatString(id),
		},
	})
	if err != nil {
		return nil, err
	}
	if resp.Item == nil {
		return nil, nil
	}

	//terms
	jwtTerm, err := util.ParseDuration(resp.Item["jwt_term"])
	if err != nil {
		return nil, err
	}
	registrationTerm, err := util.ParseDuration(resp.Item["registration_term"])
	if err != nil {
		return nil, err
	}
	recoveryTerm, err := util.ParseDuration(resp.Item["recovery_term"])
	if err != nil {
		return nil, err
	}
	//bcrypt rounds
	bcryptRounds, err := util.ParseInt(resp.Item["bcrypt_rounds"])
	if err != nil {
		return nil, err
	}

	return &domain.App{
		ID:               id,
		Origin:           util.ParseString(resp.Item["origin"]),
		Key:              util.ParseString(resp.Item["key"]),
		SigningSecret:    resp.Item["signing_secret"].B,
		BcryptRounds:     bcryptRounds,
		JWTTerm:          jwtTerm,
		RegistrationTerm: registrationTerm,
		RecoveryTerm:     recoveryTerm,
		VerificationEmail: &domain.EmailOptions{
			HTML:        util.ParseString(resp.Item["verification_email_html"]),
			Text:        util.ParseString(resp.Item["verification_email_text"]),
			Subject:     util.ParseString(resp.Item["verification_email_subject"]),
			SenderEmail: util.ParseString(resp.Item["verification_email_sender"]),
		},
		RecoveryEmail: &domain.EmailOptions{
			HTML:        util.ParseString(resp.Item["recovery_email_html"]),
			Text:        util.ParseString(resp.Item["recovery_email_text"]),
			Subject:     util.ParseString(resp.Item["recovery_email_subject"]),
			SenderEmail: util.ParseString(resp.Item["recovery_email_sender"]),
		},
	}, nil
}

//Fetch a user.
func (d *Dynamo) GetUser(appID, email string) (*domain.User, error) {
	resp, err := d.client.GetItem(&dynamodb.GetItemInput{
		TableName:      usersTable,
		ConsistentRead: aws.Bool(true),
		Key: map[string]*dynamodb.AttributeValue{
			"email":  util.FormatString(email),
			"app_id": util.FormatString(appID),
		},
	})
	if err != nil {
		return nil, err
	}
	if resp.Item == nil {
		return nil, nil
	}

	u := &domain.User{AppID: appID, Email: email}
	if v, ok := resp.Item["password"]; ok {
		u.PasswordHash = v.B
	}
	if v, ok := resp.Item["initialized"]; ok {
		t, err := util.ParseTime(v)
		if err != nil {
			return nil, err
		}
		u.Initialized = t
	}
	if v, ok := resp.Item["verified"]; ok {
		t, err := util.ParseTime(v)
		if err != nil {
			return nil, err
		}
		u.Verified = t
	}
	if v, ok := resp.Item["version"]; ok {
		i, err := util.ParseInt(v)
		if err != nil {
			return nil, err
		}
		u.Version = i
	}
	return u, nil
}

//Replace user record if version matches or insert if new. Increments version on
//success.
func (d *Dynamo) SaveUser(user *domain.User) error {
	var cond string
	var attrValues = make(map[string]*dynamodb.AttributeValue, 2)
	if user.Version == 0 {
		cond = "email <> :email AND app_id <> :app_id"
		attrValues[":email"] = util.FormatString(user.Email)
		attrValues[":app_id"] = util.FormatString(user.AppID)
	} else {
		cond = "version = :version"
		attrValues[":version"] = util.FormatInt(user.Version)
	}
	_, err := d.client.PutItem(&dynamodb.PutItemInput{
		TableName: usersTable,
		Item: map[string]*dynamodb.AttributeValue{
			"email":       util.FormatString(user.Email),
			"app_id":      util.FormatString(user.AppID),
			"password":    {B: user.PasswordHash},
			"initialized": util.FormatTime(user.Initialized),
			"verified":    util.FormatTime(user.Verified),
			"version":     util.FormatInt(user.Version + 1),
		},
		ConditionExpression:       aws.String(cond),
		ExpressionAttributeValues: attrValues,
	})
	if util.IsConditionFailed(err) {
		return log.ErrVersion
	}
	if err != nil {
		return err
	}
	user.Version++
	return nil
}
