package dynamo

import (
	"testing"

	"gitlab.com/areed/whoyou/log"
	"gitlab.com/areed/whoyou/log/test"
)

var e log.Service = New()

func TestApp(t *testing.T) {
	test.App(t, e)
}

func TestUser(t *testing.T) {
	test.User(t, e)
}
