package ephemeral

import (
	"gitlab.com/areed/whoyou/domain"
	"gitlab.com/areed/whoyou/log"
)

type userKey [2]string

//Ephemeral fulfills the Log Service interface with a store that persists until
//program execution terminates.
type Ephemeral struct {
	apps  map[string]*domain.App
	users map[userKey]*domain.User
}

//New returrns a new ephemeral store.
func New() *Ephemeral {
	return &Ephemeral{
		apps:  make(map[string]*domain.App, 16),
		users: make(map[userKey]*domain.User, 16),
	}
}

//SaveApp puts an app in the Log.
func (e *Ephemeral) SaveApp(app *domain.App) error {
	e.apps[app.ID] = app
	return nil
}

//GetApp returns the most recent argument passed to SaveApp.
func (e *Ephemeral) GetApp(appID string) (*domain.App, error) {
	app, _ := e.apps[appID]
	return app, nil
}

//SaveUser stores a user.
func (e *Ephemeral) SaveUser(user *domain.User) error {
	key := userKey{user.AppID, user.Email}
	if old, ok := e.users[key]; ok {
		if old.Version != user.Version {
			return log.ErrVersion
		}
	} else if user.Version != 0 {
		return log.ErrVersion
	}
	user.Version++
	e.users[userKey{user.AppID, user.Email}] = user
	return nil
}

//GetUser returns the most recent user passed to SaveUser.
func (e *Ephemeral) GetUser(appID, email string) (*domain.User, error) {
	if user, ok := e.users[userKey{appID, email}]; ok {
		return &domain.User{
			AppID:        user.AppID,
			Email:        user.Email,
			PasswordHash: user.PasswordHash,
			Initialized:  user.Initialized,
			Verified:     user.Verified,
			Version:      user.Version,
		}, nil
	}
	return nil, nil
}
