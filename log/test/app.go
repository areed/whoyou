package test

import (
	"testing"

	_"gitlab.com/areed/whoyou/domain"
	"gitlab.com/areed/whoyou/log"
)

func App(t *testing.T, l log.Service) {
	put := TestApp(l)
	put.Key += "x"
	err := l.SaveApp(put)
	if err != nil {
		t.Fatal(err)
	}
	got, err := l.GetApp(put.ID)
	if err != nil {
		t.Fatal(err)
	}
	if got == nil {
		t.Fatal()
	}
	if got.Origin != put.Origin {
		t.Error()
	}
	if got.Key != put.Key {
		t.Error()
	}
	if string(got.SigningSecret) != string(put.SigningSecret) {
		t.Error()
	}
	if got.BcryptRounds != put.BcryptRounds {
		t.Error()
	}
	if got.JWTTerm != put.JWTTerm {
		t.Error()
	}
	if got.RegistrationTerm != put.RegistrationTerm {
		t.Error()
	}
	if got.RecoveryTerm != put.RecoveryTerm {
		t.Error()
	}
	if got.VerificationEmail.HTML != put.VerificationEmail.HTML {
		t.Error()
	}
	if got.VerificationEmail.Text != put.VerificationEmail.Text {
		t.Error()
	}
	if got.VerificationEmail.SenderEmail != put.VerificationEmail.SenderEmail {
		t.Error()
	}
	if got.VerificationEmail.SenderName != put.VerificationEmail.SenderName {
		t.Error()
	}
	if got.VerificationEmail.Subject != put.VerificationEmail.Subject {
		t.Error()
	}
	if got.RecoveryEmail.HTML != put.RecoveryEmail.HTML {
		t.Error()
	}
	if got.RecoveryEmail.Text != put.RecoveryEmail.Text {
		t.Error()
	}
	if got.RecoveryEmail.SenderEmail != put.RecoveryEmail.SenderEmail {
		t.Error()
	}
	if got.RecoveryEmail.SenderName != put.RecoveryEmail.SenderName {
		t.Error()
	}
	if got.RecoveryEmail.Subject != put.RecoveryEmail.Subject {
		t.Error()
	}
}
