package test

import (
	"testing"

	"gitlab.com/areed/whoyou/log"
)

func User(t *testing.T, l log.Service) {
	app := TestApp(l)
	put := PendingUser(l)
	err := l.SaveUser(put)
	if err != nil {
		t.Fatal(err)
	}
	got, err := l.GetUser(app.ID, put.Email)
	if err != nil {
		t.Fatal()
	}
	if got == nil {
		t.Fatal()
	}
	if got.AppID != put.AppID {
		t.Error()
	}
	if got.Email != put.Email {
		t.Error()
	}
	if string(got.PasswordHash) != string(put.PasswordHash) {
		t.Error()
	}
	if !got.Initialized.Equal(put.Initialized) {
		t.Error()
	}
	if !got.Verified.Equal(put.Verified) {
		t.Error()
	}
	if !got.Verified.IsZero() {
		t.Error("pending user has verified timestamp")
	}
	if got.Version != put.Version {
		t.Error()
	}
	version(t, l)
}

func version(t *testing.T, l log.Service) {
	put := PristineUser(l)
	put.Version = 1
	err := l.SaveUser(put)
	if err != log.ErrVersion {
		t.Error(err)
	}
	put.Version = 0
	err = l.SaveUser(put)
	if err != nil {
		t.Fatal(err)
	}
	if put.Version != 1 {
		t.Error("SaveUser did not increment version")
	}
	got, err := l.GetUser(put.AppID, put.Email)
	if got.Version != 1 {
		t.Error("SaveUser did not bump version")
	}
	err = l.SaveUser(put)
	if err != nil {
		t.Error(err)
	}
}
