package test

import (
	"time"
	"github.com/golang/crypto/bcrypt"

	"github.com/areed/token"

	"gitlab.com/areed/whoyou/log"
	"gitlab.com/areed/whoyou/domain"
)

const TestAppID = "test"

var cachedApp *domain.App

func TestApp(l log.Service) *domain.App {
	if cachedApp != nil {
		return cachedApp
	}
	a, err := l.GetApp(TestAppID)
	if err != nil {
		panic(err)
	}
	if a != nil {
		cachedApp = a
		return a
	}
	a, err = domain.NewApp()
	if err != nil {
		panic(err)
	}
	a.ID = TestAppID
	err = l.SaveApp(a)
	if err != nil {
		panic(err)
	}
	cachedApp = a
	return a
}

const Password = "secret"

func PristineUser(l log.Service) *domain.User {
	t, err := token.New(12)
	if err != nil {
		panic(err)
	}
	hash, err := bcrypt.GenerateFromPassword([]byte(Password), TestApp(l).BcryptRounds)
	if err != nil {
		panic(err)
	}
	return &domain.User{
		AppID: TestApp(l).ID,
		Email: t + "@mailinator.com",
		PasswordHash: hash,
	}
}

func PendingUser(l log.Service) *domain.User {
	u := PristineUser(l)
	u.Initialized = time.Now()
	err := l.SaveUser(u)
	if err != nil {
		panic(err)
	}
	return u
}

func VerifiedUser(l log.Service) *domain.User {
	u := PristineUser(l)
	u.Initialized = time.Now()
	u.Verified = time.Now()
	err := l.SaveUser(u)
	if err != nil {
		panic(err)
	}
	return u
}
