package log

import (
	"errors"

	"gitlab.com/areed/whoyou/domain"
)

//Service is the type of a persistence service used by lib.
type Service interface {
	SaveApp(app *domain.App) error
	GetApp(appID string) (*domain.App, error)
	GetUser(appID, email string) (*domain.User, error)
	SaveUser(user *domain.User) error
}

var ErrVersion = errors.New("data has changed since read")
