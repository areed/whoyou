package lib

import (
	"gitlab.com/areed/whoyou/domain"
	"gitlab.com/areed/whoyou/log"
)

//should be set by program on initialization
var Log log.Service

//entities concurrently fetches a User and App from the Log. A nil app results
//in ErrNoApp, but a nil User results in a new User.
func entities(appID, email string) (app *domain.App, user *domain.User, err error) {
	//structured concurrency - no child goroutine survives beyond the
	//lifetime of its parent. http://250bpm.com/blog:71
	failures := make(chan error)
	go func() {
		var e error
		app, e = Log.GetApp(appID)
		failures <- e
	}()
	go func() {
		var e error
		user, e = Log.GetUser(appID, email)
		failures <- e
	}()
	for i := 0; i < 2; i++ {
		if err == nil {
			err = <-failures
		} else {
			<-failures
		}
	}
	if app == nil {
		return nil, nil, ErrNoApp
	}
	if user == nil {
		user = &domain.User{AppID: appID, Email: email}
	}
	return
}

//restrictions maps illegal states to descriptive errors
type restrictions map[domain.UserState]error

//forbid checks the user is not in an illegal state
func forbid(app *domain.App, user *domain.User, r restrictions) error {
	if err, ok := r[user.State(app)]; ok {
		return err
	}
	return nil
}

//wrap entities and forbid
func access(appID, email string, r restrictions) (app *domain.App, user *domain.User, err error) {
	app, user, err = entities(appID, email)
	if err != nil {
		return
	}
	err = forbid(app, user, r)
	return
}
