package lib

import (
	"github.com/golang/crypto/bcrypt"

	"github.com/areed/jwt"

	"gitlab.com/areed/whoyou/domain"
)

var authAccess = restrictions{
	domain.Pristine: ErrNotRegistered,
	domain.Pending:  ErrNotRegistered,
}

//Authenticate returns a JWT if the password is correct.
func Authenticate(appID, email, password string) ([]byte, error) {
	app, user, err := access(appID, email, authAccess)
	if err != nil {
		return nil, err
	}
	err = bcrypt.CompareHashAndPassword(user.PasswordHash, []byte(password))
	if err != nil {
		if err == bcrypt.ErrMismatchedHashAndPassword {
			return nil, ErrWrongPassword
		}
		if err == bcrypt.ErrHashTooShort {
			return nil, ErrWrongPassword
		}
		return nil, err
	}
	claims := &jwt.Claims{
		Iss: WHOYOU_ORIGIN,
		Aud: app.Origin,
		Sub: email,
	}
	jwt.Stamp(claims, app.JWTTerm)
	return jwt.Encode(claims, app.SigningSecret)
}
