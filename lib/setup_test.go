package lib

import (
	_ "gitlab.com/areed/whoyou/log/dynamo"
	"gitlab.com/areed/whoyou/log/ephemeral"
)

func init() {
	Log = ephemeral.New()
}
