package lib

import (
	"testing"

	"gitlab.com/areed/whoyou/log/test"
)

func TestChange(t *testing.T) {
	password := "my changed password"
	//Verified user
	u := test.VerifiedUser(Log)
	token, err := emailJWT(u, test.TestApp(Log).RecoveryTerm)
	if err != nil {
		t.Fatal(err)
	}
	err = Change(token, password)
	if err != nil {
		t.Fatal(err)
	}
	//check login works with new password
	_, err = Authenticate(u.AppID, u.Email, password)
	if err != nil {
		t.Error(err)
	}
	//check login fails with old password
	_, err = Authenticate(u.AppID, u.Email, test.Password)
	if err != ErrWrongPassword {
		t.Error(err)
	}

	//Pending user
	u = test.PendingUser(Log)
	token, err = emailJWT(u, test.TestApp(Log).RecoveryTerm)
	if err != nil {
		t.Fatal(err)
	}
	err = Change(token, password)
	if err != ErrNotRegistered {
		t.Error(err)
	}

	//Pristine user
	u = test.PristineUser(Log)
	token, err = emailJWT(u, test.TestApp(Log).RecoveryTerm)
	if err != nil {
		t.Fatal(err)
	}
	err = Change(token, password)
	if err != ErrNotRegistered {
		t.Error(err)
	}
}
