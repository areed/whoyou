package lib

import (
	"testing"

	"gitlab.com/areed/whoyou/domain"
	"gitlab.com/areed/whoyou/log/test"
)

func TestVerify(t *testing.T) {
	//Pending user
	u := test.PendingUser(Log)
	token, err := emailJWT(u, test.TestApp(Log).RegistrationTerm)
	if err != nil {
		t.Fatal(err)
	}
	err = Verify(token)
	if err != nil {
		t.Error(err)
	}
	u, err = Log.GetUser(u.AppID, u.Email)
	if err != nil {
		t.Fatal(err)
	}
	if u.State(test.TestApp(Log)) != domain.Verified {
		t.Error(u.State(test.TestApp(Log)))
	}

	//Pristine
	u = test.PristineUser(Log)
	token, err = emailJWT(u, test.TestApp(Log).RegistrationTerm)
	if err != nil {
		t.Fatal(err)
	}
	err = Verify(token)
	if err != ErrRegistrationNotPending {
		t.Error(err)
	}

	//Verified
	u = test.VerifiedUser(Log)
	token, err = emailJWT(u, test.TestApp(Log).RegistrationTerm)
	if err != nil {
		t.Fatal(err)
	}
	err = Verify(token)
	if err != ErrRegistered {
		t.Error(err)
	}
}
