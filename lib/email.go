package lib

import (
	"errors"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ses"

	"github.com/areed/jwt"

	"gitlab.com/areed/whoyou/domain"
)

var SES = ses.New(session.New(), &aws.Config{Region: aws.String("us-west-2")})

func send(to string, opts *domain.EmailOptions, context interface{}) error {
	html, text, err := opts.Execute(context)
	if err != nil {
		return err
	}
	return sendEmail(opts.SenderEmail, opts.SenderName, to, opts.Subject, html, text)
}

func sendEmail(from, name, to, subject, html, text string) error {
	_, err := SES.SendEmail(&ses.SendEmailInput{
		Destination: &ses.Destination{
			ToAddresses: []*string{
				aws.String(to),
			},
		},
		Message: &ses.Message{
			Body: &ses.Body{
				Html: &ses.Content{
					Data:    aws.String(html),
					Charset: aws.String("UTF-8"),
				},
			},
			Subject: &ses.Content{
				Data:    aws.String(subject),
				Charset: aws.String("UTF-8"),
			},
		},
		Source: aws.String(from),
	})
	return err
}

//generate a JWT to be emailed to verify an email address for internal calls.
func emailJWT(user *domain.User, term time.Duration) ([]byte, error) {
	claims := &jwt.Claims{
		Iss: WHOYOU_ORIGIN,
		Aud: WHOYOU_ORIGIN,
		Sub: user.Encode(),
	}
	jwt.Stamp(claims, term)
	return jwt.Encode(claims, WHOYOU_HASHING_SECRET)
}

//identify decodes an emailJWT.
func identify(token []byte) (app *domain.App, user *domain.User, err error) {
	claims, err := jwt.Decode(token, WHOYOU_HASHING_SECRET)
	if err != nil {
		return
	}
	if claims.Iss != WHOYOU_ORIGIN {
		err = errors.New("token not issued by " + WHOYOU_ORIGIN)
		return
	}
	if claims.Aud != WHOYOU_ORIGIN {
		err = errors.New("aud: " + claims.Aud + ", want: " + WHOYOU_ORIGIN)
		return
	}
	user = domain.DecodeUser(claims.Sub)
	app, user, err = entities(user.AppID, user.Email)
	return
}
