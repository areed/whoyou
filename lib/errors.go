package lib

import (
	"errors"
)

//var ErrPasswordTooShort = errors.New("ErrPasswordTooShort")
var ErrWrongPassword = errors.New("ErrWrongPassword")
var ErrRegistrationPending = errors.New("ErrRegistrationPending")
var ErrRegistrationNotPending = errors.New("ErrRegistrationNotPending")
var ErrRegistered = errors.New("ErrRegistered")
var ErrNotRegistered = errors.New("ErrNotRegistered")
var ErrNoApp = errors.New("ErrNoApp")
