package lib

import (
	"time"

	"github.com/golang/crypto/bcrypt"

	"gitlab.com/areed/whoyou/domain"
)

var registerAccess = restrictions{
	domain.Pending:  ErrRegistrationPending,
	domain.Verified: ErrRegistered,
}

//Register saves the provided credentials and sends a verification email.
func Register(appID, email, password string) error {
	app, user, err := access(appID, email, registerAccess)
	if err != nil {
		return err
	}
	hash, err := bcrypt.GenerateFromPassword([]byte(password), app.BcryptRounds)
	if err != nil {
		return err
	}
	user.PasswordHash = hash
	token, err := emailJWT(user, app.RegistrationTerm)
	if err != nil {
		return err
	}
	user.Initialized = time.Now()
	err = Log.SaveUser(user)
	if err != nil {
		return err
	}
	context := map[string]string{"URL": WHOYOU_ORIGIN + "/verify/" + string(token)}
	return send(email, app.VerificationEmail, context)
}
