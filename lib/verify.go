package lib

import (
	"time"

	"gitlab.com/areed/whoyou/domain"
)

var verifyAccess = restrictions{
	domain.Pristine: ErrRegistrationNotPending,
	domain.Verified: ErrRegistered,
}

//If successful, the user can authenticate with the email and password they
//registered with.
func Verify(token []byte) error {
	app, user, err := identify(token)
	if err != nil {
		return err
	}
	err = forbid(app, user, verifyAccess)
	if err != nil {
		return err
	}
	user.Verified = time.Now()
	return Log.SaveUser(user)
}
