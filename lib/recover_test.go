package lib

import (
	"testing"

	"gitlab.com/areed/whoyou/log/test"
)

func TestRecover(t *testing.T) {
	//Verified user
	u := test.VerifiedUser(Log)
	err := Recover(u.AppID, u.Email)
	if err != nil {
		t.Error(err)
	}
	//Pristine user
	u = test.PristineUser(Log)
	err = Recover(u.AppID, u.Email)
	if err != ErrNotRegistered {
		t.Error(err)
	}
	//Pending user
	u = test.PendingUser(Log)
	err = Recover(u.AppID, u.Email)
	if err != ErrNotRegistered {
		t.Error(err)
	}
}
