package lib

import (
	"testing"
	"time"

	"github.com/areed/jwt"

	"gitlab.com/areed/whoyou/log/test"
)

func TestAuthenticate(t *testing.T) {
	app := test.TestApp(Log)
	//Verified user
	u := test.VerifiedUser(Log)
	token, err := Authenticate(u.AppID, u.Email, test.Password)
	if err != nil {
		t.Fatal(err)
	}
	claims, err := jwt.Decode(token, app.SigningSecret)
	if err != nil {
		t.Fatal(err)
	}
	if claims.Sub != u.Email {
		t.Error("Authenticate issued a JWT for the wrong email")
	}
	if claims.Aud != app.Origin {
		t.Error("Authenticate issued a JWT for the wrong app")
	}
	if claims.Iss != WHOYOU_ORIGIN {
		t.Error("Authenticate issued a JWT from someone else")
	}
	if claims.Iat > time.Now().Unix() {
		t.Error("Authenticate issued a JWT in the future")
	}
	if claims.Exp < time.Now().Unix() {
		t.Error("Authenticate issued an expired JWT")
	}
	//wrong password
	token, err = Authenticate(u.AppID, u.Email, "x"+test.Password)
	if err != ErrWrongPassword {
		t.Error("Authenticate succeeded with an incorrect password")
	}
	if token != nil {
		t.Error("Authenticate issued a token for the incorrect password")
	}
	//unknown app
	token, err = Authenticate("x"+u.AppID, u.Email, test.Password)
	if err != ErrNoApp {
		t.Error("Authenticate succeeded with an unknown app")
	}
	if token != nil {
		t.Error("Authenticate issued a JWT for an unknown app")
	}
	//unknown email
	token, err = Authenticate(app.ID, "x"+u.Email, test.Password)
	if err != ErrNotRegistered {
		t.Error("Authenticate succeeded for an unknown user")
	}
	if token != nil {
		t.Error("Authenticate issued a JWT to an unknown user")
	}
	//Pending user
	u = test.PendingUser(Log)
	token, err = Authenticate(u.AppID, u.Email, test.Password)
	if err != ErrNotRegistered || token != nil {
		t.Error(err)
	}
	//Pristine User
	u = test.PristineUser(Log)
	token, err = Authenticate(u.AppID, u.Email, test.Password)
	if err != ErrNotRegistered {
		t.Error(err)
	}
	if token != nil {
		t.Error("Authenticate issued a JWT to a new user")
	}
}
