package lib

import (
	"gitlab.com/areed/whoyou/domain"
)

var recoverAccess = restrictions{
	domain.Pristine: ErrNotRegistered,
	domain.Pending:  ErrNotRegistered,
}

//Recover emails a user a link to change their password.
func Recover(appID, email string) error {
	app, user, err := access(appID, email, recoverAccess)
	if err != nil {
		return err
	}
	token, err := emailJWT(user, app.RecoveryTerm)
	if err != nil {
		return err
	}
	context := map[string]string{"URL": WHOYOU_ORIGIN + "/reset/" + string(token)}
	return send(email, app.RecoveryEmail, context)
}
