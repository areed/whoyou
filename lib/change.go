package lib

import (
	"github.com/golang/crypto/bcrypt"

	"gitlab.com/areed/whoyou/domain"
)

//Recover would only have succeeded for a verified user, but it's possible the
//user was deleted since the recover token was issued.
var changeAccess = restrictions{
	domain.Pristine: ErrNotRegistered,
	domain.Pending:  ErrNotRegistered,
}

//Change changes a user's password provided they have a valid emailJWT.
func Change(token []byte, password string) error {
	app, user, err := identify(token)
	if err != nil {
		return err
	}
	err = forbid(app, user, changeAccess)
	if err != nil {
		return err
	}
	hash, err := bcrypt.GenerateFromPassword([]byte(password), app.BcryptRounds)
	if err != nil {
		return err
	}
	user.PasswordHash = hash
	return Log.SaveUser(user)
}
