package lib

import (
	"testing"

	"gitlab.com/areed/whoyou/domain"
	"gitlab.com/areed/whoyou/log/test"
)

func TestRegister(t *testing.T) {
	//Pristine user
	u := test.PristineUser(Log)
	err := Register(u.AppID, u.Email, test.Password)
	if err != nil {
		t.Error(err)
	}
	u, err = Log.GetUser(u.AppID, u.Email)
	if err != nil {
		t.Fatal(err)
	}
	if u.State(test.TestApp(Log)) != domain.Pending {
		t.Error("Register did not transition user to Pending state")
	}

	//Pending user
	u = test.PendingUser(Log)
	err = Register(u.AppID, u.Email, test.Password)
	if err != ErrRegistrationPending {
		t.Fatal(err)
	}

	//Verified user
	u = test.VerifiedUser(Log)
	err = Register(u.AppID, u.Email, test.Password)
	if err != ErrRegistered {
		t.Fatal(err)
	}
}
