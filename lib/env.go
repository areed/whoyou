package lib

import "os"

var WHOYOU_HASHING_SECRET = []byte(os.Getenv("WHOYOU_HASHING_SECRET"))
var WHOYOU_ORIGIN = os.Getenv("WHOYOU_ORIGIN")

func init() {
	if string(WHOYOU_HASHING_SECRET) == "" {
		panic("WHOYOU_HASHING_SECRET is not set")
	}
	if WHOYOU_ORIGIN == "" {
		panic("WHOYOU_ORIGIN is not set")
	}
}
