package domain

import (
	"bytes"
	html "html/template"
	text "text/template"
	"time"

	"github.com/areed/token"
)

const DefaultVerificationEmailHTML = `<a href="{{.URL}}">Confirm</a>`
const DefaultVerificationEmailText = "{{.URL}}"
const DefaultVerificationEmailSubject = "Complete Your Registration"
const DefaultVerificationEmailSender = "noreply@whoyou.io"
const DefaultRecoveryEmailHTML = `<a href="{{.URL}}">Change Your Password</a>`
const DefaultRecoveryEmailText = "{{.URL}}"
const DefaultRecoveryEmailSubject = "Change Your Password"
const DefaultRecoveryEmailSender = "noreply@whoyou.io"

const DefaultBcryptRounds = 12

//The default lifetime for JWTs issued upon successful authentication is 30 days.
const DefaultJWTTerm = time.Hour * 24 * 30

//The default term for completing registration is 20 minutes.
const DefaultRegistrationTerm = 20 * time.Minute

//The default term for changing a password is 20 minutes.
const DefaultRecoveryTerm = 20 * time.Minute
const DefaultOrigin = "http://localhost:3000"

//An App is a tenant of whoyou.
type App struct {
	Origin            string
	ID                string
	Key               string
	SigningSecret     []byte
	BcryptRounds      int
	JWTTerm           time.Duration
	RegistrationTerm  time.Duration
	RecoveryTerm      time.Duration
	VerificationEmail *EmailOptions
	RecoveryEmail     *EmailOptions
}

//EmailOptions holds templates and parameters for creating emails.
type EmailOptions struct {
	htmlTmpl    *html.Template
	textTmpl    *text.Template
	HTML        string
	Text        string
	SenderEmail string
	SenderName  string
	Subject     string
}

//Execute executes a single context against the receiver's html and text
//templates. It returns the html followed by the text.
func (opts *EmailOptions) Execute(context interface{}) (string, string, error) {
	var err error
	if opts.htmlTmpl == nil {
		opts.htmlTmpl, err = html.New("html").Parse(opts.HTML)
		if err != nil {
			return "", "", err
		}
	}
	if opts.textTmpl == nil {
		opts.textTmpl, err = text.New("text").Parse(opts.Text)
		if err != nil {
			return "", "", err
		}
	}
	var htmlEmail, textEmail bytes.Buffer
	err = opts.htmlTmpl.Execute(&htmlEmail, context)
	err = opts.textTmpl.Execute(&textEmail, context)
	if err != nil {
		return "", "", err
	}
	return htmlEmail.String(), textEmail.String(), nil
}

//NewApp creates an app with a new random id, key, and secret and default values
//for all options.
func NewApp() (*App, error) {
	id, err := token.New(16)
	key, err := token.New(32)
	secret, err := token.New(64)
	if err != nil {
		return nil, err
	}
	verification := &EmailOptions{
		HTML:        DefaultVerificationEmailHTML,
		Text:        DefaultVerificationEmailText,
		SenderEmail: DefaultVerificationEmailSender,
		Subject:     DefaultVerificationEmailSubject,
	}
	recovery := &EmailOptions{
		HTML:        DefaultRecoveryEmailHTML,
		Text:        DefaultRecoveryEmailText,
		SenderEmail: DefaultRecoveryEmailSender,
		Subject:     DefaultRecoveryEmailSubject,
	}
	return &App{
		Origin:            DefaultOrigin,
		ID:                id,
		Key:               key,
		SigningSecret:     []byte(secret),
		BcryptRounds:      DefaultBcryptRounds,
		JWTTerm:           DefaultJWTTerm,
		RegistrationTerm:  DefaultRegistrationTerm,
		RecoveryTerm:      DefaultRecoveryTerm,
		VerificationEmail: verification,
		RecoveryEmail:     recovery,
	}, nil
}
