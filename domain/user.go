package domain

import (
	"strings"
	"time"
)

type UserState int

const (
	//no record of user
	Pristine UserState = iota
	//user has an open registration awaiting email verification
	Pending UserState = iota
	//user is active and has verifed their email
	Verified UserState = iota
)

//A user is uniquely identified by an email within the scope of an app.
type User struct {
	AppID        string
	Email        string
	PasswordHash []byte
	Initialized  time.Time
	Verified     time.Time
	Version      int
}

//State returns the state the user is in.
func (u *User) State(app *App) UserState {
	if u.Initialized.IsZero() {
		return Pristine
	}
	if u.Verified.IsZero() {
		if time.Since(u.Initialized) > app.RegistrationTerm {
			return Pristine
		}
		return Pending
	}
	return Verified
}

//Encode encodes a user's appID and email into a string suitable for the "sub"
//field of a JWT.
func (u *User) Encode() string {
	//a comma cannot appear in an appID and is not legal in an email
	//address.
	return u.AppID + "," + u.Email
}

//Decode reverses User.Encode. It should only be used on the "sub" field of a
//JWT produced by whoyou after the signature field has been verified because it
//will panic if it is not in the expected format.
func DecodeUser(s string) *User {
	parts := strings.Split(s, ",")
	return &User{AppID: parts[0], Email: parts[1]}
}
