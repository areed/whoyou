var fs = require('fs');
var swagger = require('swagger-tools');

var Registration = {
  type: 'object',
  properties: {
    email: {type: 'string'},
    password: {type: 'string'},
    app: {type: 'string'},
  },
  required: ['email', 'password', 'app'],
  additionalProperties: false,
};

var accounts = {
  post: {
    tags: ['accounts'],
    summary: 'Register a new account',
    description: 'Checks that the user does not already have an account then sends them a verification email.',
    operationId: 'POST /accounts',
    parameters: [{
      name: 'payload',
      'in': 'body',
      schema: {
        '$ref': '#/definitions/Registration',
      },
    }],
    responses: {
      '200': {
        description: 'Verification email has been sent.',
        headers: {
          'Access-Control-Allow-Origin': {type: 'string'},
        },
      },
      '400': {
        description: 'Bad Request',
        headers: {
          'Access-Control-Allow-Origin': {type: 'string'},
          'Content-Type': {type: 'string'},
        },
      },
      '500': {
        description: 'Internal Server Error',
        headers: {
          'Access-Control-Allow-Origin': {type: 'string'},
          'Content-Type': {type: 'string'},
        },
      },
    },
    'x-amazon-apigateway-integration': {
      type: 'aws',
      uri: 'arn:aws:apigateway:us-west-2:lambda:path/2015-03-31/functions/arn:aws:lambda:us-west-2:290766561564:function:whoyou-' + process.env.CI_BUILD_TAG + '/invocations',
      httpMethod: 'POST',
      credentials: 'arn:aws:iam::290766561564:role/APIGatewayLambdaExecRole',
      requestTemplates: {
        'application/json': '{"id": "$context.requestId", "method": "User.Register", "params": [{"Email": $input.json(\'$.email\'), "Password": $input.json(\'$.password\'), "AppID": $input.json(\'$.app\')}]}'
      },
      responses: {
        'default': {
          statusCode: '200',
          responseParameters: {
            'method.response.header.Access-Control-Allow-Origin': "'*'",
          },
        },
        'Err.*': {
          statusCode: '400',
          responseParameters: {
            'method.response.header.Access-Control-Allow-Origin': "'*'",
            'method.response.header.Content-Type': "'text/plain'",
          },
        },
        '^(?!Err)(.|\\n)+': {
          statusCode: '500',
          responseParameters: {
            'method.response.header.Access-Control-Allow-Origin': "'*'",
            'method.response.header.Content-Type': "'text/plain'",
          },
        },
      },
    },
  },
};

var spec = {
  swagger: '2.0',
  info: {
    title: 'whoyou.io',
    description: 'JWTs as a service.',
    version: '1.0.0',
  },
  host: 'whoyou.io',
  schemes: ['https'],
  paths: {
    '/accounts': accounts
  },
  definitions: {
    Registration: Registration,
  },
};

swagger.specs.v2.validate(spec, function(err, data) {
  if (err) {
    throw err;
  }
  if (data) {
    data.errors.forEach(log);
    data.warnings.forEach(log);
    throw new Error('invalid schema');
  }
  fs.writeFileSync('swagger.json', JSON.stringify(spec));
});

function log(x) {
  console.log(x);
}
