var test = require('tape');
var jwt = require('webcrypto-jwt');

const API_KEY = 'xyz';
const API_SECRET = 'f238e92537408733500e16683a20bd786eafc2bdfc44bc179b3de827e53f89d2';

test('POST /accounts (register)', function(t) {
  t.test('Single uninterrupted request', {skip: false, timeout: 60000}, function(t) {
    var creds = fakeCreds();

    register(creds, {
      onload: function() {
        t.equal(this.status, 200, 'response status code is 200');
        checkJWT(t, this.response, creds.email, function() {
          authenticate(creds, {
            onload: function() {
              t.equal(this.status, 200, 'response status codes is 200');
              checkJWT(t, this.response, creds.email);
            },
          });
        });
      },
    });
    alert('click verification link emailed to ' + creds.email);
  });
});

test('Interrupted registration with open request', {skip: false, timeout: 60000}, function(t) {
  var creds = fakeCreds();

  register(creds, {
    ontimeout: function() {
      register(creds, {
        onload: function() {
          t.equal(this.status, 200, 'response status code is 200');
          checkJWT(t, this.response, creds.email);
        },
      });
    },
    timeout: 1000,
  });
  alert('close then click verification link emailed to ' + creds.email);
});

test('Completed registration while disconnected', {skip: false, timeout: 60000}, function(t) {
  t.comment('User has submitted email/password and verified email, but the client app has never received a 200 response to POST /accounts for the email.');

  t.test('Same password used for registration', function(t) {
    t.test('Registration is active.', function(t) {
      var creds = fakeCreds();

      register(creds, {
        timeout: 1000,
      });

      setTimeout(function() {
        alert('before closing, click verification link emailed to ' + creds.email);
        register(creds, {
          onload: function() {
            t.equal(this.status, 200, 'response status code is 200');
            checkJWT(t, this.response, creds.email);
          },
        });
      }, 1500);
    });

    t.test('Registration is expired.', {skip: true}, function(t) {
      t.end();
      //start registration over
    });
  });

  t.test('Different password from registration record', {skip: true}, function(t) {
    t.test('Registration is active.', function(t) {
      t.end();
      //Forbidden error
    });

    t.test('POST /accounts with different password after registration is expired.', {skip: true}, function(t) {
      t.end();
      //start registration over
    });
  });

  t.test('Sign-in attempts with correct credentials', {skip: true}, function(t) {
    //404
  });
});

test('Inter-app duplicate email', {skip: true, timeout: 60000}, function(t) {

});

/* ERRORS */

test('Intra-app duplicate email', {skip: true, timeout: 60000}, function(t) {

});

test('Register without Accept header', {skip: true, timeout: 60000}, function(t) {

});

test('Register with unparseable Accept header', {skip: true, timeout: 60000}, function(t) {

});

test('Register with unacceptable content-type', {skip: true, timeout: 60000}, function(t) {

});

test('Register with unknown Api Key', {skip: true, timeout: 60000}, function(t) {

});

test('Register without email', {skip: true, timeout: 60000}, function(t) {

});

test('Register without password', {skip: true, timeout: 60000}, function(t) {

});

test('Register with unparseable body', {skip: true, timeout: 60000}, function(t) {

});

function register(creds, config) {
  var xhr = new XMLHttpRequest();
  xhr.open('POST', 'https://localhost/accounts');
  xhr.setRequestHeader('Accept', accept('xyz'));
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  Object.assign(xhr, config);
  xhr.send(encode(creds.email, creds.password));
}

function authenticate(creds, config) {
  var xhr = new XMLHttpRequest();
  xhr.open('POST', 'https://localhost/jwts');
  xhr.setRequestHeader('Accept', accept('xyz'));
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  Object.assign(xhr, config);
  xhr.send(encode(creds.email, creds.password));
}

function accept(app) {
  return ['text/vnd', app, 'whoyou.io+plain'].join('.');
}

function encode(email, password) {
  return [
    'email=',
    encodeURIComponent(email),
    '&password=',
    encodeURIComponent(password)
  ].join('');
}

function checkJWT(t, token, email, next) {
  jwt.verifyJWT(token, API_SECRET, 'HS256', function(err, ok) {
    t.error(err, 'jwt verification error');
    var claims = jwt.parseJWT(token);
    t.equal(claims.sub, email, 'jwt claims sub matches user email');
    t.equal(claims.iss, 'whoyou.io', 'jwt claims iss "whoyou.io"');
    t.equal(claims.aud, 'http://localhost:8000', 'jwt claims aud "http://localhost:8000"');
    t.ok(claims.exp > (Date.now() / 1000), 'token is not expired');
    if (typeof next === 'function') next();
    else t.end();
  });
}

function fakeCreds() {
  return {
    email: [token(), token(), '@mailinator.com'].join(''),
    passowrd: [token(), token(), token()].join(''),
  };
}

function token() {
  return Math.random().toString(32).substring(2);
}
