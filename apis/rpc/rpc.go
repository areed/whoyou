package rpc

import (
	"gitlab.com/areed/whoyou/lib"
)

//User has methods applicable to a single user of an app.
type User struct{}

//PublicArgs contains parameters passed in by calls originating from an app.
type PublicArgs struct {
	AppID    string
	Email    string
	Password string
}

//PrivateArgs contains parameters passed in by calls originating from whoyou.
type PrivateArgs struct {
	Token    string
	Password string
}

//Register wraps lib.Register. No response.
func (u *User) Register(args PublicArgs, reply *struct{}) error {
	return lib.Register(args.AppID, args.Email, args.Password)
}

//Verify wraps lib.Verify. No response.
func (u *User) Verify(args PrivateArgs, reply *struct{}) error {
	return lib.Verify([]byte(args.Token))
}

//Authenticate wraps lib.Authenticate, returning a JWT string to the client.
func (u *User) Authenticate(args PublicArgs, reply *string) error {
	//put the returned jwt into the reply
	jwt, err := lib.Authenticate(args.AppID, args.Email, args.Password)
	if err != nil {
		return err
	}
	*reply = string(jwt)
	return nil
}

//Recover wraps lib.Recover. No response.
func (u *User) Recover(args PublicArgs, reply *struct{}) error {
	return lib.Recover(args.AppID, args.Email)
}

//Change wraps lib.Change. No Response.
func (u *User) Change(args PrivateArgs, reply *struct{}) error {
	return lib.Change([]byte(args.Token), args.Password)
}
